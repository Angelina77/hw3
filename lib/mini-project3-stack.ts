import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class MiniProject3Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // create a new S3 bucket
    const bucket = new cdk.aws_s3.Bucket(this, 'MyEncryptedBucket', {
      //Add bucket properties like versioning and encryption
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED,
    });
  }
}
